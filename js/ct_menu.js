jQuery(document).ready(function($) {
    
	//this provides the ajax refresh of the views we have in the menu...
	//D9 patches have broken the feature formally using the Drupal API, now we delete and recreate the view manually.
	Drupal.behaviors.refreshCtMenuViews = {
		attach: function (context, settings) {
			$('#drupal-off-canvas').find('form.node-policy-form .alert-success, form.node-policy-edit-form .alert-success, form.node-goal-target-form .alert-success, form.node-goal-target-edit-form .alert-success').once('updated-view').each( function() {
				$( ".view-conservation-tracking-menu" ).trigger('RefreshView');
				$("div.ui-dialog-titlebar button.ui-dialog-titlebar-close").delay( 800 ).trigger('click');
			});
			$('#drupal-off-canvas').find('form.node-indicator-form .alert-success, form.node-indicator-edit-form .alert-success, form.node-indicator-data-glo-form .alert-success, form.node-indicator-data-regional-form .alert-success, form.node-indicator-data-country-form .alert-success, form.node-indicator-data-local-form .alert-success').once('updated-view').each( function() {
				$( ".view-conservation-tracking-menu" ).trigger('RefreshView');
				if ($('#block-indicatorcard:visible').length){
					closeIndicatorCard();
					showIndicatorCard(currentIndicatorNodeURL);
				}
				$("div.ui-dialog-titlebar button.ui-dialog-titlebar-close").delay( 800 ).trigger('click');
			});	
		}
	};

    Drupal.behaviors.addCtMenuSearchEvents = {
		attach: function (context, settings) {
            $(context).find("#menu-indicator-search-cards-wrapper").once("add-search-behaviours").each(function () {
                addCTMenuSearchFunctions();
                var searchInput = $( "#ct-menu-search-filter .form-type-textfield input" );
                searchInput.focus(function() {
                    $( "div#menu-indicator-count" ).hide(200);
                    $( "div#ct-menu-search-count" ).show(200);
                    $( "div#menu-indicator-search-cards-wrapper" ).slideDown(200);
                });
                searchInput.focusout(delay(function (e) {
                    $( "div#menu-indicator-count" ).show(200);
                    $( "div#ct-menu-search-count" ).hide(200);
                    $( "div#menu-indicator-search-cards-wrapper" ).slideUp(200);
                }, 100));
                searchInput.focus();
                var tmpStr = searchInput.val();
                searchInput.val('');
                searchInput.val(tmpStr);
            });
		}
	};

    Drupal.behaviors.addCtMenuViewInteraction = {
		attach: function (context, settings) {
            $(context).find("#menu-indicator-cards-wrapper").once("add-menu-interaction").each(function () {
                addCTMenuInteraction();
            });
		}
	};

	//$(indicatorContainer).append("<div id='mini-loader-wrapper'><div id='mini-loader'></div></div>");
    if($("#menu-container").length){
        $( "#menu-container" ).ready(function() {
            addCTMenuInteraction();
            addCTMenuSearchFunctions();
        });
    }

    function addCTMenuSearchFunctions() {
        var searchTextField = $("#ct-menu-search-filter div.form-type-textfield input");
        var hiddenMenuSearchField = $( "#ct-menu-filters div.form-type-textfield input" );
        var hiddenSearchButton = $( "#ct-menu-search-filter .form-actions input.form-submit" );
        var menuSearchButton = $( "#ct-menu-filters .form-actions input.form-submit" );

        searchTextField.focus(function() {
            $("div#menu-indicator-search-cards-wrapper").slideDown(200);
        });
        searchTextField.keyup(delay(function (e) {
            hiddenSearchButton.trigger( "click" );
            hiddenMenuSearchField.val($(this).val().substring(0, 100));
            if (e.key === 'Enter' || e.keyCode === 13) {
                menuSearchButton.trigger( "click" );
            }
        }, 500));
        var tempVal= searchTextField.val().substring(0, 100);
        $('div#menu-indicator-search-cards .views-row .search-indicator-text').hover(
            function() {
                var indicatorSearchText = $(this).text().trim().substring(0, 100);
                searchTextField.val(indicatorSearchText);
                hiddenMenuSearchField.val(indicatorSearchText);
                return null;
            }, function () {
                searchTextField.val(tempVal);
                hiddenMenuSearchField.val(tempVal);
                return null;
            }
        );
        $('.search-policy-trigger').hover(
            function() {
                var indicatorSearchText = $(this).find(".search-policy-text").text().trim();
                searchTextField.val(indicatorSearchText);
                hiddenMenuSearchField.val(indicatorSearchText);
            }, function () {
                searchTextField.val(tempVal);
                hiddenMenuSearchField.val(tempVal);
            }
        );
        $('.search-target-trigger').hover(
            function() {
                var indicatorSearchText = $(this).find(".search-target-text").text().trim();
                searchTextField.val(indicatorSearchText);
                hiddenMenuSearchField.val(indicatorSearchText);
            }, function () {
                searchTextField.val(tempVal);
                hiddenMenuSearchField.val(tempVal);
            }
        );
        $('.indicator-search-card, .search-target-trigger').click( function() {
            $('div#menu-indicator-search-cards .views-row .search-indicator-text, .search-target-trigger, .search-policy-trigger').off(); //removes the events so the text doesn't change 
            $( "#ct-menu-search-filter .form-actions input.form-submit, #ct-menu-filters .form-actions input.form-submit" ).delay(200).trigger( "click" );
        });
    }

    function addCTMenuInteraction() {
        $('div.indicator-card .menu-open-indicator').once("open-indicator").on( 'click', function() {
            //$('div.indicator-card .menu-open-indicator').off('click');
            $(this).closest(".menu-open-indicator-wrapper").addClass("button-is-loading-indicator");
            var nodeID = $(this).closest("div.indicator-card").find(".indicator-nid").text().trim();
            $("i.fa-minus").removeClass("fa-minus").addClass("fa-plus"); //change the icon of any potentially open indicators
            $(this).children("i").removeClass("fa-plus").addClass("fa-minus");
            currentIndicatorNodeURL = '/indicator_card/'+nodeID;
            showIndicatorCard(currentIndicatorNodeURL);
        });
    }

    function delay(callback, ms) {
        var timer = 0;
        return function() {
          var context = this, args = arguments;
          clearTimeout(timer);
          timer = setTimeout(function () {
            callback.apply(context, args);
          }, ms || 0);
        };
    }

    function showIndicatorCard(indicatorURL){
        
        $(".button-is-loading-indicator").append("<div id='map-loader-wrapper'><div id='mini-loader'></div></div>");
        $(".button-is-loading-indicator").removeClass("button-is-loading-indicator");
        $("#block-indicatorcard").empty();
        
        var ajaxSettings = {
            url: indicatorURL,
            wrapper: 'block-indicatorcard',
            method: 'append',
          };
          var myAjaxObject = Drupal.ajax(ajaxSettings);
          myAjaxObject.execute();
          $("#block-indicatorcard").show();
    }
});