
//

function makeDoughnutChart(){
	var indicatorURL = selData.chart.RESTurl;
	var chartColors = ['#5470c6', '#91cc75', '#fac858', '#ee6666', '#73c0de', '#3ba272', '#fc8452', '#9a60b4', '#ea7ccc'];
	if (indicatorURL.includes("observatoire-comifac.net")){
		chartColors = ['#8fc04f', '#ae0000'];
	}

	chartSettings = checkChartData("doughnut");
	console.log(selData);
	option = {
	  legend: {
		top: 'bottom'
	  },
	  color: chartColors,
	  toolbox: biopamaGlobal.chart.toolbox,
	  tooltip: {
		trigger: 'item',
		formatter: '{b}  {c}' + chartSettings.unit
	  },
	  series: [
		{
		  type: 'pie',
		  radius: chartSettings.radius,
		  center: ['50%', '50%'],
		  roseType: chartSettings.roseType,
		  itemStyle: {
			borderRadius: 8
		  },
		  data: chartSettings.data
		}
	  ]
	};
	indicatorChart.setOption(option); 
	return;
}