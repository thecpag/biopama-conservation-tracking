var indicatorAjaxRequest = '';

jQuery(document).ready(function($) {
    var map;
    if($("#map-container").length){ //we are checking if the map div is the one from the CTT page
        map = $().createMap("map-container");

        $().addMapControls(map, "satelliteToggle");
        $().addMapControls(map, "zoom");
        $().addMapControls(map, "hoveredAreaLabelBlock");
        $().addMapControls(map, "paPolyFillControl");
        $().addMapControls(map, "loaderControl");
        $().addMapControls(map, "fullScreen");
        $().addMapControls(map, "navigation");	
    
        $().addMapLayerBiopamaSources(map);
            
        $().addMapLayer(map, "biopamaGaulEez");
        $().addMapLayer(map, "biopamaRegions");
        $().addMapLayer(map, "biopamaCountries");
        $().addMapLayer(map, "biopamaWDPAPolyJRC");
        $().addMapLayer(map, "biopamaWDPAPoint");
        $().addMapLayer(map, "satellite");
        $().addMapLayer(map, "CountriesRedGreen");
        $().addMapLayer(map, "CountriesGoodBad");
        $().addMapLayer(map, "nan-layers");

        // var popupOptions = {
        //     countryLink: '/ct/country/',
        //     paLink: '/ct/pa/',
        // }
        // $().addMapLayerInteraction(map, popupOptions);

    } else { // if it's not the CTT div we can assume we are on the Country/PA page and use that map instead.
        map = mymap;
    };
	
	

	
	
	$('input').each(function(i){
	  if(this.id){
		this.id = this.id+i;
		$(this).closest('form').addClass(this.id);
	  }
	});

	map.on('moveend', function () {
		//this flag can only be true in this case if the Protected Area has been changed to one in a different country from the search
		if (countryChanged === 1){	
			updateCountry();
		}
		if (paChanged === 1){	
			updatePa();
		}
		if (regionChanged === 1){	
			updateRegion();
		}
	});
	
	map.on('load', function () {
        if (window.location.search.indexOf('?search=') > -1) { // check if we got here with a search term in the URL (to open an indicator by default)
            $("#menu-indicator-cards .indicator-card:first .menu-open-indicator").click(); //find the open indicator button of the first search result and click it.
        }
		$('body').toggleClass('loaded').delay( 500 ).queue(function() {
		  $('.mapboxgl-ctrl.ajax-loader').toggle(false);
		  mapPostLoadOptions();
		  $( this ).dequeue();
		});
	});

	function mapPostLoadOptions() {
		map.setMinZoom(1.4);
		map.setMaxZoom(16);
	}

});

function getRestResults(){
    jQuery().insertBiopamaLoader(".indicator-chart"); 
    firstChartRun = 1;
    var dataCountry = 1;
    //delete any errors that might be up, if they persist, they will be re-added
    jQuery( ".rest-error" ).empty();
    //if the chosen indicator has countries attached to it we highlight them, and mask the ones not included.
    if (selData.info.countries != ''){
        thisMap.setFilter("CountriesBadMask", buildFilter(selData.info.countries, '!in', 'iso3'));
        thisMap.setFilter("CountriesGoodMask", buildFilter(selData.info.countries, 'in', 'iso3'));
        thisMap.setLayoutProperty("CountriesGoodMask", 'visibility', 'visible');
        thisMap.setLayoutProperty("CountriesBadMask", 'visibility', 'visible');
    }
    if (selData.data.countries.length > 0){
        if (selData.data.countries.indexOf(selSettings.ISO3) > -1){
            // a country or countries have been set and the current country is in the set
            dataCountry = 1;
        } else {
            //a country or countries have been set and the currently selected country is not one of them
            dataCountry = 0;
        }
    }
    if (dataCountry == 1){

        //generate url by replacing tokens that might be in it.
        var indicatorURL = '';
        indicatorURL = jQuery().biopamaReplaceTokens(selData.chart.RESTurl, selSettings);  
        
        indicatorAjaxRequest = jQuery.ajax({
            url: indicatorURL,
            dataType: 'json',
            success: function(d) {
                jQuery().removeBiopamaLoader(".indicator-chart"); 
                if (d.hasOwnProperty("records")){ //from a JRC REST Service
                    selData.chart.RESTResults = d.records;
                    if (d.metadata.recordCount == 0) {
                        //we create a card, but tell it that the response was empty (error 2)
                        getChart(2);
                    } else {
                        //the 0 means there was no error
                        getChart(0);
                    }
                } else if (d.hasOwnProperty(selData.chart.RESTdataContext)){ //single value passed, found in root, good to go.
                    selData.chart.RESTResults = d[selData.chart.RESTdataContext];
                    getChart(0);
                } else if (selData.chart.RESTdataContext !== ''){
                    if (indicatorURL.includes("observatoire-comifac.net")){
                        var OFACCountryIso3 = ["CMR","COD","GAB","TCD","GNQ","RWA","BDI","STP","CAF","COG"];
                        var OFACCountryNames = ["Cameroon","Congo (the Democratic Republic of the)","Gabon","Chad","Equatorial Guinea","Rwanda","Burundi","São Tomé and Príncipe","Central African Republic","Republic of the Congo"];
                        var countryArray = jmespath.search(d, 'data.*.eligible_country[]');

                        //needed as the ofac-comifac services change between an array and an obj.
                        if(typeof countryArray[0] !== 'string'){
                            countryArray = jQuery().getKeys(countryArray[0]); 
                        }

                        var countryNamesIn = [];
                        var countryMapIn = ['in', 'iso3'];
                        var countryNamesOut = [];
                        var countryMapOut = ['in', 'iso3'];
                        
                        OFACCountryIso3.forEach(function (value, index) {
                            if (countryArray.includes(value)) {
                                countryNamesIn.push(OFACCountryNames[index]);
                                countryMapIn.push(OFACCountryIso3[index]);
                            } else {
                                countryNamesOut.push(OFACCountryNames[index]);
                                countryMapOut.push(OFACCountryIso3[index]);
                            }
                        });

                        var countryNamesInString = countryNamesIn.join(", ");
                        var countryNamesOutString = countryNamesOut.join(", ");

                        thisMap.setFilter("CountriesGreenMask", countryMapIn);
                        thisMap.setLayoutProperty("CountriesGreenMask", 'visibility', 'visible');
                        thisMap.setFilter("CountriesRedMask", countryMapOut);
                        thisMap.setLayoutProperty("CountriesRedMask", 'visibility', 'visible');
                        if (countryNamesInString){
                            jQuery( '<p style="color: #8fc04f;"><b>Pays atteignant l\'objectif: </b>'+ countryNamesInString + '</p>' ).insertAfter( "#indicator-chart-regional" );
                        } else {
                            jQuery( '<p style="color: #858585;"><b>Aucun pays n\'a atteint cet objectif.</b></p>' ).insertAfter( "#indicator-chart-regional" );
                        }
                    
                    }
                    //var match1 = jsonPath(d, selData.chart.RESTdataContext); jsonPath works too, but JMES has better documentation
                    var match1 = jmespath.search(d, selData.chart.RESTdataContext);
                    //console.log(match1);
                    selData.chart.RESTResults = match1;
                    getChart(0);
                } else {
                    selData.chart.RESTResults = d;
                    getChart(0);
                }
            },
            error: function() {
                jQuery().removeBiopamaLoader(".indicator-chart"); 
                console.log("ERROR")
                //we create a card, but tell it that there was a general error (error 1)
                //todo - expand error codes to tell user what went wrong.
                getChart(1);
            }
        });
    } else {
        jQuery().removeBiopamaLoader(".indicator-chart"); 
        //we run the get chart function, only to show the user that a different country must be selected
        getChart(3);
    }
    //
}