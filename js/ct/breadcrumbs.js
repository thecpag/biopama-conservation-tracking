function buildBreadcrumbCard(URL){
	return;
}

function removeCountry(){
	selSettings.countryName = 'trans-ACP';
	selSettings.ISO2 = null;
	selSettings.ISO3 = null;
	selSettings.NUM = null;
 	thisMap.setFilter('wdpaAcpFill', null);
	highlightMapFeature();
	jQuery('.mapboxgl-ctrl-z-country').hide();
	thisMap.setFilter("wdpaAcpPolyLabels", ["in", "Point", 0]);
	thisMap.setFilter("wdpaAcpPointLabels", ["in", "Point", 1]);
	thisMap.setLayoutProperty("countrySelected", 'visibility', 'none');
	thisMap.setLayoutProperty("wdpaAcpFillHighlighted", 'visibility', 'none');
	if (selSettings.paName !== 'default'){
		removePA();
	} else {
		//updateAddress();
	}
}

function removeRegion(){
	selSettings.regionID = null;
	selSettings.regionName = null;
	highlightMapFeature();
	thisMap.setFilter('wdpaAcpFill', null);
	thisMap.setFilter('countryFill', null);
	thisMap.setFilter('regionsFill', null);
	thisMap.setLayoutProperty("regionsFill", 'visibility', 'visible');
	jQuery('.mapboxgl-ctrl-z-region').hide();
	if (selSettings.ISO2 !== null){
		removeCountry();
	} else {
		//updateAddress();
	}
	
	thisMap.setLayoutProperty("countryFill", 'visibility', 'none');
	thisMap.setLayoutProperty("regionSelected", 'visibility', 'none');
}

function removePA(){
	selSettings.paName = 'default';
	selSettings.WDPAID = 0;
	//this clears the PA from the chart if a chart is visible
	highlightMapFeature();
	jQuery('.mapboxgl-ctrl-z-pa').hide();
	thisMap.setLayoutProperty("wdpaAcpSelected", 'visibility', 'none');
	//updateAddress();
	//disable the indicator card tab.. if there is no tab nothing will happen
}

