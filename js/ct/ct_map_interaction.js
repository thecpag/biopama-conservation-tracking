jQuery(document).ready(function($) {
	var closePopupIcon = undefined;
	//alternative interesting events 'boxzoomend', 'zoomend', 'touchend'
	//thisMap.on('click', getFeatureInfo);
	var currentPath = window.location.pathname;
	if(!currentPath.includes("ct/country/") && !currentPath.includes("ct/pa/")){
		thisMap.on('click', getFeatureInfo);
	} 
	
	var popUpContents = {};
  
	function getFeatureInfo(e){
		popUpContents = {
			region: '',
			regionID: '',
			country: '',
			countryCode: '',
			country3Code: '',
			PAs: [],
			WDPAIDs: [],
			paIUCNCats: [],
			paSize: [],
		};
		new Promise(function(resolve, reject) {
		  regionCheck(e)
		}).then(countryCheck(e))
		.then(paCheck(e))
		.then(makePopUp(e));
	}
	
	function makePopUp(e){
		if (popUpContents.regionID.length){
			var areaCheck = "areaInactive";
			if (selSettings.regionName == popUpContents.region)areaCheck = "areaActive";
			var popup = popUpSection(areaCheck, popUpContents.region, popUpContents.regionID, "region");
			var regionPopup = new mapboxgl.Popup({anchor:'right', className: 'biopamaPopup', offset: 40, closeButton: false})
				.setLngLat(e.lngLat)
				.setHTML(popup)
				.addTo(thisMap);
		} 
		
		if (popUpContents.countryCode.length){
			var areaCheck = "areaInactive";
			if (selSettings.ISO2 == popUpContents.countryCode)areaCheck = "areaActive";
			var popup = popUpSection(areaCheck, popUpContents.country, popUpContents.countryCode, "country");
			var countryPopup = new mapboxgl.Popup({anchor:'bottom', className: 'biopamaPopup', offset: 40, closeButton: false})
				.setLngLat(e.lngLat)
				.setHTML(popup)
				.addTo(thisMap);
		} 

		var numberOfPas = "singlePA";
		var paPopupContent = '';
		if (popUpContents.PAs.length){
			if (popUpContents.PAs.length == 1){
				checkPA(popUpContents.WDPAIDs[0], 0)
			} else {
				numberOfPas = "multiPAs"
				for (var key in popUpContents.PAs) {
					checkPA(popUpContents.WDPAIDs[key], key)
				}
			}
			var paPopup = new mapboxgl.Popup({anchor:'left', className: 'biopamaPopup '+numberOfPas, offset: 40, closeButton: false})
				.setLngLat(e.lngLat)
				.setHTML(paPopupContent)
				.addTo(thisMap);
		}
		function checkPA(paID, paKey){
			var areaCheck = "areaInactive";
			if (paID == selSettings.WDPAID)areaCheck = "areaActive";
			paPopupContent = paPopupContent + popUpSection(areaCheck, popUpContents.PAs[paKey], popUpContents.WDPAIDs[paKey], "pa");
		}
		
		// create a HTML element for the centre
		var el = document.createElement('div');
		el.className = 'mapCloseMarker';
		el.innerHTML = '<i class="fas fa-times"></i>';
		if (closePopupIcon !== undefined){
			closePopupIcon.remove();
		}
		
		closePopupIcon = new mapboxgl.Marker(el)
			.setLngLat(e.lngLat)
			.addTo(thisMap);	
			
		$( "a.cardGoTo" ).tooltip({
			trigger:"hover",
			html: true,
			placement: "top",
			title:"Open in a new window",
			container: 'body',
			delay: 100,
		});
		$( "div.cardSelect" ).tooltip({
			trigger:"hover",
			html: true,
			placement: "top",
			title:"Select this area",
			container: 'body',
			delay: 100,
		});

		$( "div.mapCloseMarker" ).click(function(e) {
			e.stopPropagation();
			closeMapCards();
		});
		$('.cardTooltip').on('show.bs.tooltip', function () {
			$('.tooltip').remove();
		})
		$( "div.region div.cardSelect" ).click(function(e) {
			selSettings.regionName = popUpContents.region;
			selSettings.regionID = popUpContents.regionID;
			updateRegion(selSettings.regionID);
			closeMapCards();
		});
		$( "div.country div.cardSelect" ).click(function(e) {
			selSettings.ISO2 = popUpContents.countryCode;
			console.log(popUpContents.countryCode)
			selSettings.regionID = popUpContents.regionID;
			//if (selSettings.regionID !== popUpContents.regionID) regionChanged = 1;
			updateCountry();
			closeMapCards();
		});
		$( "div.pa div.cardSelect" ).click(function(e) {
			selSettings.WDPAID = $(this).attr('id');
			selSettings.paName = $(this).closest(".card").find(".area-subheading").text().trim();
			selSettings.ISO3 = popUpContents.country3Code;
			selSettings.ISO2 = popUpContents.countryCode;
			updatePa();
			closeMapCards();
		});

		$( "div.card.region" ).hover(function(e) {
			thisMap.setFilter('regionHover', ['==', 'Group', popUpContents.region]);		
			thisMap.setLayoutProperty("regionHover", 'visibility', 'visible');	
		});
		$( "div.card.country" ).hover(function(e) {
			thisMap.setFilter('countryHover', ['==', 'iso3', popUpContents.country3Code]);		
			thisMap.setLayoutProperty("countryHover", 'visibility', 'visible');	
		});
		$( "div.card.pa" ).hover(function(e) {
			var wdpaID = parseInt($(this).attr('id'), 10);
			thisMap.setFilter('wdpaAcpHover', ['==', 'wdpaid', wdpaID]);		
			thisMap.setLayoutProperty("wdpaAcpHover", 'visibility', 'visible');	
		});
		function closeMapCards(){
			if (countryPopup !== undefined){
				regionPopup.remove();
				countryPopup.remove();
			}
			if (paPopup !== undefined)paPopup.remove();
			closePopupIcon.remove();
			$('.tooltip').remove();
		}
		function popUpSection(activeStatus, cardTitle, spatialID, spatialScope){
			var areaIcons = '';
			if(spatialScope !== 'region'){
				areaIcons = '<div class="area-icons">'+
					'<a href="/ct/'+spatialScope+'/'+spatialID+'" class="cardIcon cardTooltip cardGoTo" target="_blank"><i class="fas fa-arrow-right"></i></a>'+
				'</div>';
			}
			var tempCard = '<div class="card homepageMapCard '+ activeStatus +' ' + spatialScope + '" id="'+spatialID+'">'+
				'<div class="card-header card-header-'+ spatialScope +'">'+ spatialScope + '</div>'+
				'<div class="card-body homepageMapCardBody cardSelect" id="'+spatialID+'">'+
					'<div class="row">'+
						'<div class="col-12">'+
							'<h4 class="area-subheading">' + cardTitle + '</h4>'+
						'</div>'+
					'</div>'+
				'</div>'+
				areaIcons +
			'</div>';
			return tempCard;
		}
	}
	
	function regionCheck(e){
		var region = getMapRegion(e.point);
		//console.log(region);
		if (typeof region !== 'undefined') {
			popUpContents.region = region.properties.Group;
			switch(popUpContents.region) {
				case "Eastern Africa":
					popUpContents.regionID = "eastern_africa";
					break;
				case "Central Africa":
					popUpContents.regionID = "central_africa";
					break;
				case "Western Africa":
					popUpContents.regionID = "western_africa";
					break;
				case "Southern Africa":
					popUpContents.regionID = "southern_africa";
					break;
				default:
					popUpContents.regionID = popUpContents.region;
					break;
				}
		} 
		return;
	}
	function countryCheck(e){
		var country = getMapCountry(e.point);
		if (typeof country !== 'undefined') {
			popUpContents.country = country.properties.name_iso31;
			popUpContents.countryCode = country.properties.iso2; //For the Country Page
			popUpContents.country3Code = country.properties.iso3; //For PA feature matching
		}
		return;
	}

	function paCheck(e){
		var PAs = getMapPAs(e.point);
		if (typeof PAs !== 'undefined') {
			for (var key in PAs) {
				popUpContents.PAs.push(PAs[key].properties.name);
				popUpContents.WDPAIDs.push(PAs[key].properties.wdpaid);
				popUpContents.paIUCNCats.push(PAs[key].properties.iucn_cat);
				popUpContents.paSize.push(PAs[key].properties.gis_area);
				if (PAs[key].properties.wdpaid == selSettings.WDPAID){

				}
			}		
		}
		return;
	}
});

//returns the first country object
function getMapRegion(point){
	var feature = thisMap.queryRenderedFeatures(point, {
		layers:["regionsMask"],
	});
	//as long as we have something in the feature query 
	if (typeof feature[0] !== 'undefined'){
		return feature[0];
	} 
}

//returns the first country object
function getMapCountry(point){
	var feature = thisMap.queryRenderedFeatures(point, {
		layers:["countryMask"],
	});
	//as long as we have something in the feature query 
	if (typeof feature[0] !== 'undefined'){
		return feature[0];
	}
}

//returns a list of PA objects
function getMapPAs(point){
	var paFeatures = thisMap.queryRenderedFeatures(point, {
		layers:["wdpaAcpMask"]
	});
	if (typeof paFeatures[0] !== 'undefined'){
		return paFeatures;
	}
}
