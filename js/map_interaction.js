function updateRegion(region = selSettings.regionID, countryUpdate = true, zoomTo = true) {
	regionChanged = 0;
	if((selSettings.ISO2 !== null) && (countryUpdate === true)){
		removeCountry();
	}
	if (zoomTo) {	
		jQuery().mapZoomToRegion(thisMap, region);
		if (jQuery(".indi-tab-regional[aria-expanded='true']:visible").length) {
			getRestResults();
		}
	 	
	}
	thisMap.setLayoutProperty("regionSelected", 'visibility', 'visible');
	thisMap.setFilter('regionSelected', ['==', 'Group', selSettings.regionName]);
	//updateAddress();
	//updateBreadRegion();
	jQuery('.mapboxgl-ctrl-z-region').show();
	
}

function updateCountry(chartValue = 'iso2', zoomTo = true, clearPA = true) {
	countryChanged = 0;
	
	//If we are moving to a new country the currently selected PA can't follow us there, so we remove it
	if((clearPA === true) && (selSettings.WDPAID > 0)){ removePA(); }
	
	//Our Protected areas layer does not have the ISO2 country codes, it only has ISO3 codes.
	//The mapbox geocoder only has ISO2 codes (in lower case)...
	//We use our country layer, which has both, as a lookup table
	var relatedFeatures = thisMap.querySourceFeatures("BIOPAMA_Poly",{
		sourceLayer: biopamaGlobal.map.layers.country,
		filter: ['==', 'iso2', selSettings.ISO2]
	});
	//Here we update all of our global settings by getting them from the EEZ/Country layer
	selSettings.ISO2 = relatedFeatures[0].properties.iso2;
	selSettings.ISO3 = relatedFeatures[0].properties.iso3;
	selSettings.NUM = relatedFeatures[0].properties.un_m49;
	selSettings.countryName = relatedFeatures[0].properties.name_iso31;

	if (selSettings.regionName !== relatedFeatures[0].properties.Group){
		selSettings.regionName = relatedFeatures[0].properties.Group;
		updateRegion(selSettings.regionName, false, false);
	}

	if (zoomTo) {
		var currentPath = window.location.pathname;
		if(!currentPath.includes("ct/country/") && !currentPath.includes("ct/pa/")){
			jQuery().mapZoomToCountryIso2(thisMap, selSettings.ISO2);
			updateAddress();
		}
	}
	
	if (jQuery(".indi-tab-national[aria-expanded='true']:visible").length) {
		getRestResults();
	}
	thisMap.setFilter("wdpaAcpPolyLabels", [ "all", ["in", "Point", 0], ['in', 'ISO3', selSettings.ISO3] ]);
	thisMap.setFilter("wdpaAcpPointLabels", [ "all", ["in", "Point", 1], ['in', 'ISO3', selSettings.ISO3] ]);
	thisMap.setFilter('countryFill', [ "all", ['==', 'Group', selSettings.regionName], ['!=', 'iso3', selSettings.ISO3] ]);
	thisMap.setLayoutProperty("countrySelected", 'visibility', 'visible');
	thisMap.setFilter('countrySelected', ['==', 'iso3', selSettings.ISO3]);
	thisMap.setFilter('wdpaAcpFillHighlighted', ['==', 'iso3', selSettings.ISO3]);
	thisMap.setFilter('wdpaAcpFill', ['!=', 'iso3', selSettings.ISO3]);
	jQuery('.mapboxgl-ctrl-layer-fill').show();
	thisMap.setLayoutProperty("wdpaAcpFillHighlighted", 'visibility', 'visible');
	jQuery('.mapboxgl-ctrl-z-country').show();
}
function updatePa() {
	paChanged = 0;
	thisMap.setLayoutProperty("wdpaAcpSelected", 'visibility', 'visible');
	thisMap.setFilter('wdpaAcpSelected', ['==', 'wdpaid', selSettings.WDPAID]);
	thisMap.setPaintProperty('wdpaAcpSelected', 'line-width', 10);
	setTimeout(function(){
		thisMap.setPaintProperty('wdpaAcpSelected', 'line-width', 2);
	}, 300);
	//if there's a chart 
	if (jQuery("#indicator-chart-country:visible").length){
		highlightMapFeature();
	}

	updateCountry('iso3', false, false);
	if (jQuery(".indi-tab-local[aria-expanded='true']:visible").length) {
		getRestResults(); //if we are not in the tab and it's not turned off, switch to it.
	}

	jQuery('.mapboxgl-ctrl-z-pa').show();
	//udateAddress();
	jQuery().mapZoomToPA(thisMap, selSettings.WDPAID);
}
