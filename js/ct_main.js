jQuery(document).ready(function($) {
    Drupal.behaviors.biopama_ct_progressbar = {
        attach: function(context, settings) {
       
            $(document).ajaxComplete(function (event, xhr, settings) {
                $('.ct-progress-bar').hide();
            });   

            $('#country-selector').on('change', function() {
                selSettings.ISO2 = $(this).val();
                countryChanged = 1;             
                $().mapZoomToCountryIso2(thisMap, selSettings.ISO2);
            });

        }
    };
    Drupal.Ajax.prototype.beforeSubmit = function (form_values, element, options) {
        $('.ct-progress-bar').show();
    };

    Drupal.theme.ajaxProgressThrobber = function () {
        $('.ct-progress-bar').show();
        return;
    };
});